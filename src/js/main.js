import _ from 'lodash';
import axios from 'axios';

window.addEventListener('DOMContentLoaded', () => {
  'use strict';

  const root = document.getElementById('root'),
    checkboxs = document.querySelectorAll('input[type="checkbox"]'),
    preloader = document.querySelector('.loadingio-spinner-dual-ring-4i4348iac9');
  preloader.style.display = 'block';
  let base_url = 'https://petstore.swagger.io/v2';



  const renderProducts = (data) => {
    root.innerHTML = '';
    data.forEach((item) => {
      const cart = document.createElement('div');
      cart.classList.add('product');
      cart.setAttribute('data-status', item.status);
      cart.setAttribute('data-id', item.id);

      cart.innerHTML = `
          <p class="product__name">${item.name}</p>
          ${item.status == "available" ? '<button class="product__buy button">Buy</button>' : ''}
        `;
      root.append(cart);
    });
  };

  const buyProduct = () => {
    const cart = document.querySelectorAll('.product');
    cart.forEach(item => {
      item.addEventListener('click', (e) => {
        if (e.target.closest('.product__buy')) {
          let id = item.getAttribute('data-id');

          const buyObj = {
            id: id,
            petId: id,
            quantity: '1',
            status: "placed",
            complete: true
          }

          let question = confirm('Are you sure you want to buy?');
          if (question) {
            axios.post(`${base_url}/store/order`, buyObj)
              .then(alert('Thank you - your order has been accepted'))
              .catch(error => alert('An error has occurred' + error));
          }
        }
      })
    })
  }

  const loadProducts = () => {
    let statuses = [];

    if(checkboxs[0].checked) statuses.push('available')
    if(checkboxs[1].checked) statuses.push('pending')
    if(checkboxs[2].checked) statuses.push('sold')
    if(!checkboxs[0].checked && !checkboxs[1].checked && !checkboxs[2].checked) statuses.push('available,pending,sold')

    preloader.style.display = 'block';
    axios.get(`${base_url}/pet/findByStatus?status=${statuses.join(',')}`)
      .then(({data}) => {
        preloader.style.display = 'none';
        renderProducts(_.uniqBy(data, 'id'));
        buyProduct();
      })
      .catch(error => console.log(error));
  };
  loadProducts();

  checkboxs.forEach(checkbox => {
    checkbox.addEventListener('change', () => {
      loadProducts();
    });
  });
});
